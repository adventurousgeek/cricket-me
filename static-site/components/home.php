<!--- Landing Section -->

<section class="main-screen">
    <div class="container">
        <header class="row">
            <div class="col-md-3">
                <ul>
                    <li>Betting</li>
                    <li>How it Works</li>
                </ul>
            </div>
            <div class="col-md-6">
                <h1>Logo</h1>
            </div>
            <div class="col-md-3">
                <ul>
                    <li>About</li>
                    <li>Team</li>
                    <li>Contact</li>
                </ul>
            </div>
        </header>
        <div class="row">
            <div class="col-md-12">
                <h1>Blockchain Powered, Personalised Peer-to-Peer Cricket Betting Exchange with Auto-settlements of Betting Contracts.</h1>
                <button type="button" class="btn btn-outline-primary">Whitepaper</button>
                <button type="button">Scroll</button>
            </div>
        </div>
    </div>
</section>

<section class="what-is">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Introduction</h3>
                <h2>What is Cricket.me?</h2>
                <p>Cricket.me is a one stop destination portal for people interested in cricket.</p>
                <p>People can follow matches and players, discuss topics related to cricket and bet on match outcomes.</p>
                <div class="row">
                    <div class="col-md-4">
                        <p>Cricket Updates</p>
                    </div>
                    <div class="col-md-4">
                        <p>P2P Content</p>
                    </div>
                    <div class="col-md-4">
                        <p>P2P Betting</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h1>Countdown to Crowdsale</h1>
            </div>
            <div class="col-md-6">
                <p>Countdown: 37 44 48 40</p>
                <p>Days Hrs Mins Sec</p>
            </div>
            <div class="colmd-3">
                <h2>Join Presale</h2>
            </div>
        </div>
    </div>
</section>

<section class="problem-solution">
    <div class="container">
        <div class="row"><div class="col-md-12"><h3>Betting</h3></div></div>
        <div class="row">
            <div class="col-md-6">
                <h2>Problem</h2>
            </div>
            <div class="col-md-6">
                <h2>Solution</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div id="accordion" role="tablist" aria-multiselectable="true">
                    <div>
                        <div role="tab" id="headingOne">
                            <h5 class="mb-0">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Untimely Payments
                                </a>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div>
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <div>
                        <div role="tab" id="headingTwo">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Possible Bad Faith
                                </a>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div>
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <div>
                        <div role="tab" id="headingThree">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Restricted Methods
                                </a>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div>
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <div>
                        <div role="tab" id="headingThree">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                                    No Cricket Platform
                                </a>
                            </h5>
                        </div>
                        <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div>
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                Atom and Hand
            </div>
            <div class="col-md-4">
                <div id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="card">
                        <div class="card-header" role="tab" id="headingOne">
                            <h5 class="mb-0">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseOne">
                                    Timely Payments
                                </a>
                            </h5>
                        </div>

                        <div id="collapseFive" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="card-block">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" role="tab" id="headingTwo">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseTwo">
                                    P2P Content Creation
                                </a>
                            </h5>
                        </div>
                        <div id="collapseSix" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="card-block">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" role="tab" id="headingThree">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseThree">
                                    Smart Contracts
                                </a>
                            </h5>
                        </div>
                        <div id="collapseSeven" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="card-block">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" role="tab" id="headingThree">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseThree">
                                    Cricket Platform
                                </a>
                            </h5>
                        </div>
                        <div id="collapseEight" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="card-block">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="how-it-works">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <p>Roadmap</p>
                <h2>How it Works</h2>
            </div>
            <div class="col-md-8">

            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h3>Head-to-Head</h3>
                <h4>Late Q2 2018</h4>
                <p>Cricket.me blockchain mediates predictions between two players taking opposite sides on upcoming sporting events. The network fee for H2H bets is 2 percent of the payout. Half of this fee is distributed to the Oracle Masternode network for processing the transaction. The remaining half gets automatically destroyed.</p>
            </div>
            <div class="col-md-4">
                <h3>Multi-User</h3>
                <h4>Early Q4 2018</h4>
                <p>To increase liquidity on low volume events, the multi-user system allow multiple players to be matched against a single bettor. This ensures that large bets do not require an exact match to forge a contract. The network fee for this contract is 4 percent of the payout. Half the fee goes to the Oracle processing the transaction.  Half is destroyed.</p>
            </div>
            <div class="col-md-4">
                <h3>Fantasy Sports</h3>
                <h4>Late Q1 2019</h4>
                <p>Users assemble virtual teams from players of professional sports leagues, then use Wagerr to bet on the outcomes of fantasy matches. Based on the real life performance of actual players, winning predictions are paid out automatically when Oracle Masternodes report consensus on performance statistics.</p>
            </div>
        </div>
    </div>
</section>

<section class="team">

</section>

<section class="main-footer">
</section>