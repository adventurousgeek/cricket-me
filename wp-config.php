<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cricket_me');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'YGT*%jA{T.tZ|$c5`9pGV!8k<X}mI76?JiQV?x6+F-0D8`j^W<%I7}G+F<h:B*0A');
define('SECURE_AUTH_KEY',  'zccMo<luK&90D9T@~ob}FV6/,V4?#=c<{6?7x1UG+bc7Ssl5z7fE#2dpbf^6A]2N');
define('LOGGED_IN_KEY',    '9aH}OfL?|V^qO5 OF_Z:,<A1vo+BVJ^W6&|1mWW_p3wRkO46`+*@@Sf9|I|Mc~37');
define('NONCE_KEY',        'Uqp>,#VnA<$q!ZIT+K^FND3p7&Mz_3{+^ z&zsrD1t`cI[Eo)>6yb<.YnNsR!8s_');
define('AUTH_SALT',        'JrB!8=hta3aM,S-uZr.OG25977BUf0bk@/QG{zxX|+SL/N% ^%>NA.!1m,wS@gz)');
define('SECURE_AUTH_SALT', 'W&r2WQL}nfne3!g8Dr##SY>E^IMbiE=ml8yu+5i&jT>GW<4z y+lLU(LZB-lNDt9');
define('LOGGED_IN_SALT',   'mkYdo XcX;@f%$$F?Ed}rBxD{p~.tNpSXeW) /7F&gznpHQ8w#p64x]T}uL}kUQT');
define('NONCE_SALT',       'R(AI+o`Fg{hXQQa ,a<[v?(=mL/xmZ*wYZF^9J/^(5([YmJ5)QB>w hT(:At_5IY');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
